# 0.0.6
+ Added package `external`
+ Added `(*Object).GetString()`
+ Modified keywords `MKDIR`, `MOV`, `REM`, `GETENV` and `SETENV` to utilize the above changes.
+ Added keywords:
    * `RUN "<command>"`
    * `RUN.Q "<command>"`
    * `RUN.BG "<command>"`

# 0.0.5
+ Added keywords:
    * `MKDIR "<name>"`
    * `MOV "<old name>" "<new name>"`
    * `REM "<name>"`
    * `SETENV "<variable>" "<value>"`;
+ Added a (buggy) REPL, accessible via `screeptist !REPL`;
+ Changed the way the file byte buffer gets converted into a string, preventing possible spam of null characters. 