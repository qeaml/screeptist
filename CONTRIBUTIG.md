# Internal structure

* `core` - Contains the Object, constants and expressions - add new object types here.
* `external` - Interactions with the outside world - executables, environment variables, file/directory management.
* `generic` - Contains a generic parser - add your keywords here.
* `parser` - Contains the parser and statement runner.

# Naming rules

## Variables

All variables are generaly named in camelcase.
Important/global variables' names must be fully capitalized and must begin with a `!`.

### Examples

Correct variable names
```
SET variable
SET alsoVariable
SET !IMPORTANT_VARIALBE
SET !ALSOIMPORTANT          // discouraged
```

Allowed, but not recommended
```
SET some_variable
SET some/variable
SET some:variable
SET some.variable

SET !SOME/VARIABLE
SET !SOME:VARIABLE
SET !SOME.VARIABLE
```

## Keywords

All keywords' names should be fully capital, or at least their first letter. 
Variants of keywords must contain their parent keyword, preferably separated with a `.`.
The length of the keywords is *recommended* to be 3 to 6 letters for stylistic consistency.

### Examples

Examples of correct keyword names
```
SET
ADD
SUB
MUL

// also allowed
Set
Add
Sub
Mul

// not allowed
set
add
sub
mul
```

Examples of keyword variants
```
// regular call
CALL
// call in background via goroutine
CALL.BG
// same as above
CALLBG
BGCALL
CALL_IN_BG  // discouraged
```

# Keyword workings

## Return type

The return type of the keyword is affected by the amount of arguments is accepts and their types.

### 0 arguments or non-numerical arguments or mixed arguments

The return type can be any.

### 1 numerical argument

The return type must be the same as the input type, unless a conversion is necessary.

Examples: `INC`, `DEC`

### 2+ numerical arguments

The return type must be the same as the first argument, unless a conversion is necessary.

Examples: `ADD`, `SUB`, `MUL`, `DIV`