package external

import (
	"fmt"
	"os/exec"
	"strings"
)

// Run executes a command in directory Path
func Run(command, path string) error {
	head := strings.SplitN(command, " ", 1)[0]
	tail := strings.Join(strings.Split(command, " ")[1:], " ")
	cmd := exec.Command(head, tail)
	cmd.Dir = path
	out, err := cmd.Output()
	if err != nil {
		return err
	}
	fmt.Println(string(out))
	return nil
}

// RunQuiet does the same as Run, just without printing
func RunQuiet(command, path string) error {
	head := strings.SplitN(command, " ", 1)[0]
	tail := strings.Join(strings.Split(command, " ")[1:], " ")
	cmd := exec.Command(head, tail)
	cmd.Dir = path
	_, err := cmd.Output()
	if err != nil {
		return err
	}
	return nil
}
