package external

import (
	"os"
)

// GetEnv returns an environment variable's value
func GetEnv(name string) (string, bool) {
	value := os.Getenv(name)
	return value, value != ""
}

// SetEnv sets an environment variable
func SetEnv(name, value string) error {
	return os.Setenv(name, value)
}

// HasEnv returns whether an environment variable exists
func HasEnv(name string) bool {
	_, ok := GetEnv(name)
	return ok
}
