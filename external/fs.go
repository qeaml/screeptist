package external

import (
	"os"
	"strings"
)

const sep string = string(os.PathSeparator)

// IsAbsolutePath returns true if the path specified is an absolute path
func IsAbsolutePath(path string) bool {
	return strings.Contains(path, sep)
}

// GetAbsolutePath returns an absolute directory given a path and current working directory
func GetAbsolutePath(path, dir string) string {
	if IsAbsolutePath(path) {
		return path
	}
	if path == "." {
		return dir
	}
	if path == ".." {
		splitDir := strings.Split(dir, sep)
		splitDir = splitDir[0 : len(splitDir)-1]
		return strings.Join(splitDir, sep)
	}
	return dir + sep + path
}

// MakeDir creates a directory
func MakeDir(name, path string) error {
	return os.Mkdir(GetAbsolutePath(name, path), os.ModePerm)
}

// MakeFile creates a blank file
func MakeFile(name, path string) error {
	_, err := os.Create(GetAbsolutePath(name, path))
	return err
}

// Rename renames a file or directory
func Rename(oldname, path, newname string) error {
	oldpath := GetAbsolutePath(oldname, path)
	newpath := GetAbsolutePath(newname, path)
	return os.Rename(oldpath, newpath)
}

// Remove removes a file or directory
func Remove(name, path string) error {
	return os.Remove(GetAbsolutePath(name, path))
}
