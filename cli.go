package main

import (
	"bufio"
	"fmt"
	"os"
	"screeptist/core"
	"screeptist/generic"
	"strings"
)

func main() {
	fmt.Println("screeptist ", core.VERSION)
	p := generic.GetParser()
	if len(os.Args) < 2 {
		fmt.Println("no file specified")
	} else {
		if os.Args[1] == "!REPL" {
			var err error
			reader := bufio.NewReader(os.Stdin)
			for {
				fmt.Print("-> ")
				text, _ := reader.ReadString('\n')
				text = strings.Replace(text, "\n", "", -1)
				if strings.Compare("QUIT", text) == 0 {
					break
				} else {
					err = p.Parse(text)
					if err != nil {
						fmt.Println("ERR: ", err)
					}
				}
			}
		} else {
			err := p.RunFile(os.Args[1])
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}
