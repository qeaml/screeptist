package generic

import (
	"fmt"
	"os"
	"path/filepath"
	"screeptist/core"
	"screeptist/external"
	"screeptist/parser"
	"strings"
)

// operations
func eq(a *core.Object, b *core.Object) (bool, error) {
	return *a == *b, nil
}

func neq(a *core.Object, b *core.Object) (bool, error) {
	return *a != *b, nil
}

func lt(a *core.Object, b *core.Object) (bool, error) {
	if a.IsFloat() && b.IsFloat() {
		return a.FloatValue < b.FloatValue, nil
	} else if a.IsFloat() && b.IsInt() {
		return a.FloatValue < float32(b.IntValue), nil
	} else if a.IsInt() && b.IsInt() {
		return a.IntValue < b.IntValue, nil
	} else if a.IsInt() && b.IsFloat() {
		return a.IntValue < int32(b.FloatValue), nil
	} else {
		return false, fmt.Errorf(
			"unsupported types: `%s` and `%s`",
			a.TypeName(), b.TypeName())
	}
}

func gt(a *core.Object, b *core.Object) (bool, error) {
	if a.IsFloat() && b.IsFloat() {
		return a.FloatValue > b.FloatValue, nil
	} else if a.IsFloat() && b.IsInt() {
		return a.FloatValue > float32(b.IntValue), nil
	} else if a.IsInt() && b.IsInt() {
		return a.IntValue > b.IntValue, nil
	} else if a.IsInt() && b.IsFloat() {
		return a.IntValue > int32(b.FloatValue), nil
	} else {
		return false, fmt.Errorf(
			"unsupported types: `%s` and `%s`",
			a.TypeName(), b.TypeName())
	}
}

// keywords
func show(args []parser.Argument, vars *parser.VarMap) error {
	//fmt.Println("varmap: ", *vars)
	for _, a := range args {
		if a.IsRaw {
			val, ok := (*vars)[a.Raw]
			if !ok {
				return fmt.Errorf("could not find variable `%s`", a.Raw)
			}
			fmt.Println(parser.Repr(val))
		} else {
			fmt.Println(parser.Repr(a.Value))
		}
	}
	return nil
}

func set(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name  string
		value *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	if args[1].IsRaw {
		value = (*vars)[args[1].Raw]
	} else {
		value = args[1].Value
	}
	(*vars)[name] = value
	return nil
}

func void(args []parser.Argument, vars *parser.VarMap) error {
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	(*vars)[args[0].Raw] = core.NewObject()
	return nil
}

func load(args []parser.Argument, vars *parser.VarMap, p *parser.Parser) error {
	var filename string
	if args[0].IsRaw {
		val, ok := (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		if val.IsString() {
			filename = val.StringValue
		} else {
			return fmt.Errorf("expected string variable but got `%s`",
				val.TypeName())
		}
	} else {
		val := args[0].Value
		if val.IsString() {
			filename = val.StringValue
		} else {
			return fmt.Errorf("expected string variable but got `%s`",
				val.TypeName())
		}
	}
	if !strings.HasSuffix(filename, ".ree") {
		filename += ".ree"
	}
	return p.RunFile(filename)
}

func print(args []parser.Argument, vars *parser.VarMap) error {
	for _, a := range args {
		if a.IsRaw {
			val, ok := (*vars)[a.Raw]
			if !ok {
				return fmt.Errorf("could not find variable `%s`", a.Raw)
			}
			if val.IsString() {
				fmt.Println(val.StringValue)
			} else {
				fmt.Println(parser.Repr(val))
			}
		} else {
			if a.Value.IsString() {
				fmt.Println(a.Value.StringValue)
			} else {
				fmt.Println(parser.Repr(a.Value))
			}
		}
	}
	return nil
}

func add(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	tmp = core.NewObject()
	if lvalue.IsInt() {
		if rvalue.IsInt() {
			tmp.SetInt(lvalue.IntValue + rvalue.IntValue)
		} else if rvalue.IsFloat() {
			tmp.SetInt(lvalue.IntValue + int32(rvalue.FloatValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else if lvalue.IsFloat() {
		if rvalue.IsFloat() {
			tmp.SetFloat(lvalue.FloatValue + rvalue.FloatValue)
		} else if rvalue.IsInt() {
			tmp.SetFloat(lvalue.FloatValue + float32(rvalue.IntValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else {
		return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
	}
	(*vars)[name] = tmp
	return nil
}

func sub(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	tmp = core.NewObject()
	if lvalue.IsInt() {
		if rvalue.IsInt() {
			tmp.SetInt(lvalue.IntValue - rvalue.IntValue)
		} else if rvalue.IsFloat() {
			tmp.SetInt(lvalue.IntValue - int32(rvalue.FloatValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else if lvalue.IsFloat() {
		if rvalue.IsFloat() {
			tmp.SetFloat(lvalue.FloatValue - rvalue.FloatValue)
		} else if rvalue.IsInt() {
			tmp.SetFloat(lvalue.FloatValue - float32(rvalue.IntValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else {
		return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
	}
	(*vars)[name] = tmp
	return nil
}

func mul(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	tmp = core.NewObject()
	if lvalue.IsInt() {
		if rvalue.IsInt() {
			tmp.SetInt(lvalue.IntValue * rvalue.IntValue)
		} else if rvalue.IsFloat() {
			tmp.SetInt(lvalue.IntValue * int32(rvalue.FloatValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else if lvalue.IsFloat() {
		if rvalue.IsFloat() {
			tmp.SetFloat(lvalue.FloatValue * rvalue.FloatValue)
		} else if rvalue.IsInt() {
			tmp.SetFloat(lvalue.FloatValue * float32(rvalue.IntValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else {
		return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
	}
	(*vars)[name] = tmp
	return nil
}

func div(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	if rvalue.IsInt() {
		if rvalue.IntValue == 0 {
			return fmt.Errorf("cannot divide by zero")
		}
	} else if rvalue.IsFloat() {
		if rvalue.FloatValue == 0.0 {
			return fmt.Errorf("cannot divide by zero")
		}
	}
	tmp = core.NewObject()
	if lvalue.IsInt() {
		if rvalue.IsInt() {
			tmp.SetFloat(float32(lvalue.IntValue) / float32(rvalue.IntValue))
		} else if rvalue.IsFloat() {
			tmp.SetFloat(float32(lvalue.IntValue) / rvalue.FloatValue)
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else if lvalue.IsFloat() {
		if rvalue.IsFloat() {
			tmp.SetFloat(lvalue.FloatValue / rvalue.FloatValue)
		} else if rvalue.IsInt() {
			tmp.SetFloat(lvalue.FloatValue / float32(rvalue.IntValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else {
		return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
	}
	(*vars)[name] = tmp
	return nil
}

func mod(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	if rvalue.IsInt() {
		if rvalue.IntValue == 0 {
			return fmt.Errorf("cannot divide by zero")
		}
	} else if rvalue.IsFloat() {
		if rvalue.FloatValue == 0.0 {
			return fmt.Errorf("cannot divide by zero")
		}
	}
	tmp = core.NewObject()
	if lvalue.IsInt() {
		if rvalue.IsInt() {
			tmp.SetInt(lvalue.IntValue % rvalue.IntValue)
		} else if rvalue.IsFloat() {
			tmp.SetInt(lvalue.IntValue % int32(rvalue.FloatValue))
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else if lvalue.IsFloat() {
		if rvalue.IsFloat() {
			tmp.SetInt(int32(lvalue.FloatValue) % int32(rvalue.FloatValue))
		} else if rvalue.IsInt() {
			tmp.SetInt(int32(lvalue.FloatValue) % rvalue.IntValue)
		} else {
			return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
		}
	} else {
		return fmt.Errorf("unexpected type `%s`", rvalue.TypeName())
	}
	(*vars)[name] = tmp
	return nil
}

func concat(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name   string
		lvalue *core.Object
		rvalue *core.Object
		ok     bool
		tmp    *core.Object
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	lvalue, ok = (*vars)[name]
	if !ok {
		return fmt.Errorf("could not find variable `%s`", name)
	}
	if args[1].IsRaw {
		rvalue = (*vars)[args[1].Raw]
	} else {
		rvalue = args[1].Value
	}
	tmp = core.NewObject()
	if !lvalue.IsString() {
		return fmt.Errorf("expected string but got `%s`", lvalue.TypeName())
	}
	if !rvalue.IsString() {
		return fmt.Errorf("expected string but got `%s`", rvalue.TypeName())
	}
	tmp.SetString(lvalue.StringValue + rvalue.StringValue)
	(*vars)[name] = tmp
	return nil
}

func nav(args []parser.Argument, vars *parser.VarMap) error {
	var (
		dest string
		tmp  *core.Object
		ok   bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		dest, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[1].Raw)
		}
	} else {
		dest, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[1].Raw)
		}
	}
	dest = external.GetAbsolutePath(dest, (*vars)["!PATH"].StringValue)
	(*vars)["!PATH"].SetString(dest)
	return nil
}

func getenv(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name     string
		envname  string
		envvalue string
		tmp      *core.Object
		ok       bool
	)
	if !args[0].IsRaw {
		return fmt.Errorf(
			"expected word, but got `%s` instead",
			args[0].Value.TypeName())
	}
	name = args[0].Raw
	if args[1].IsRaw {
		tmp, ok = (*vars)[args[1].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[1].Raw)
		}
		envname, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		envname, ok = args[1].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	tmp = core.NewObject()
	envvalue, ok = external.GetEnv(envname)
	if !ok {
		return fmt.Errorf("could not find environment variable `%s`", envname)
	}
	tmp.SetString(envvalue)
	(*vars)[name] = tmp
	return nil
}

func setenv(args []parser.Argument, vars *parser.VarMap) error {
	var (
		envname  string
		envvalue string
		tmp      *core.Object
		ok       bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		envname, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		envname, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	if args[1].IsRaw {
		tmp, ok = (*vars)[args[1].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[1].Raw)
		}
		envvalue, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		envvalue, ok = args[1].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.SetEnv(envname, envvalue)
}

func mkdir(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name string
		tmp  *core.Object
		ok   bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		name, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		name, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.MakeDir(name, (*vars)["!PATH"].StringValue)
}

func mov(args []parser.Argument, vars *parser.VarMap) error {
	var (
		aname string
		bname string
		tmp   *core.Object
		ok    bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		aname, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		aname, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	if args[1].IsRaw {
		tmp, ok = (*vars)[args[1].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[1].Raw)
		}
		bname, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		bname, ok = args[1].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.Rename(aname, (*vars)["!PATH"].StringValue, bname)
}

func rem(args []parser.Argument, vars *parser.VarMap) error {
	var (
		name string
		tmp  *core.Object
		ok   bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		name, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		name, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.Remove(name, (*vars)["!PATH"].StringValue)
}

func run(args []parser.Argument, vars *parser.VarMap) error {
	var (
		command string
		tmp     *core.Object
		ok      bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		command, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		command, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.Run(command, (*vars)["!PATH"].StringValue)
}

func runQ(args []parser.Argument, vars *parser.VarMap) error {
	var (
		command string
		tmp     *core.Object
		ok      bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		command, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		command, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	return external.RunQuiet(command, (*vars)["!PATH"].StringValue)
}

func runBg(args []parser.Argument, vars *parser.VarMap) error {
	var (
		command string
		tmp     *core.Object
		ok      bool
	)
	if args[0].IsRaw {
		tmp, ok = (*vars)[args[0].Raw]
		if !ok {
			return fmt.Errorf("could not find variable `%s`", args[0].Raw)
		}
		command, ok = tmp.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	} else {
		command, ok = args[0].Value.GetString()
		if !ok {
			return fmt.Errorf("expected string but got `%s`", tmp.TypeName())
		}
	}
	go external.Run(command, (*vars)["!PATH"].StringValue)
	return nil
}

// GetParser returns a generic parser with operations, keywords and variables
func GetParser() *parser.Parser {
	var tmp *parser.Parser = parser.NewParser()
	core.AddOperator("=", eq)
	core.AddOperator("~", neq)
	core.AddOperator("<", lt)
	core.AddOperator(">", gt)
	parser.AddKw("SHOW", show)
	parser.AddKw("SET", set)
	parser.AddKw("VOID", void)
	parser.AddKw("PRINT", print)
	parser.AddKw("ADD", add)
	parser.AddKw("SUB", sub)
	parser.AddKw("MUL", mul)
	parser.AddKw("DIV", div)
	parser.AddKw("MOD", mod)
	parser.AddKw("CONCAT", concat)
	parser.AddKw("NAV", nav)
	parser.AddKw("GETENV", getenv)
	parser.AddKw("SETENV", setenv)
	parser.AddKw("MKDIR", mkdir)
	parser.AddKw("MOV", mov)
	parser.AddKw("REM", rem)
	parser.AddKw("RUN", run)
	parser.AddKw("RUN.Q", runQ)
	parser.AddKw("RUN.BG", runBg)
	parser.AddSkw("LOAD", load)
	tmp.Parse("SET !VER \"" + core.VERSION + "\"")
	tmp.Parse("VOID !NULL")
	exe, err := os.Executable()
	if err != nil {
		fmt.Println("WARN: could not determine executable path")
	} else {
		tmp.Parse("SET !PATH \"" + filepath.Dir(exe) + "\"")
	}
	return tmp
}
